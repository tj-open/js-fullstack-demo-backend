/* eslint import/no-extraneous-dependencies:off */

const chalk = require('chalk');

require('babel-register');
require('regenerator-runtime/runtime');

if (process.listenerCount('unhandledRejection') <= 0) {
  process.on('unhandledRejection', (err) => {
    console.error(chalk.red('unhandledRejection:', err.stack || err.toString()));
    process.exit(1);
  });
}

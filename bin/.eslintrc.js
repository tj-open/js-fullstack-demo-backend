module.exports = {
  "settings": {
    "import/resolver": {
      "node": {
        "moduleDirectory": ["node_modules", "src"]
      }
    }
  },
  "rules": {
    "import/no-extraneous-dependencies": "off"
  }
}
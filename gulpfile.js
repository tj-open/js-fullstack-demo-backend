/* eslint-disable import/no-extraneous-dependencies, promise/always-return, consistent-return */

require('promise-addition');
const path = require('path');
const through = require('through2');
const chalk = require('chalk');
const newer = require('gulp-newer');
const babel = require('gulp-babel');
const sourcemaps = require('gulp-sourcemaps');
const gulp = require('gulp');
const del = require('del');

const currProjectDir = process.cwd();

exports.paths = [
  { src: ['src/**/*'], dest: 'dist' },
];

exports.clean = function clean() {
  return del(exports.paths.map(p => p.dest));
};

function babelify(src, dest) {
  const options = process.argv.slice(3);
  const useSourcemap = options.indexOf('-s') >= 0 || options.indexOf('--source-maps') >= 0;
  if (useSourcemap) console.log('Building with `--source-maps inline` option...');

  let stream = gulp.src(src)
    // .pipe(through.obj((file, encode, callback) => {
    //   file._path = file.path;
    //   file.path = file.path.replace(reSrcFile, reDestFile);
    //   callback(null, file);
    // }))
    .pipe(newer(dest))
    .pipe(through.obj((file, encode, callback) => {
      console.log('Building', `'${chalk.cyan(path.relative(currProjectDir, file.path))}'...`);
      callback(null, file);
    }));

  if (useSourcemap) stream = stream.pipe(sourcemaps.init());
  stream = stream.pipe(babel({
    only: [/.*\.js$/, /.*\.es$/],
    // ignore: ['*.json'],
  }));
  if (useSourcemap) {
    stream = stream.pipe(sourcemaps.write());
    // .pipe(sourcemaps.write('.')); // if you want `--source-maps both`, uncomment this line
  }

  stream = stream.pipe(gulp.dest(dest));
  stream.on('error', console.error);
  return stream;
}
exports.babelify = babelify;

exports.build = function build(done) {
  const tasks = exports.paths.map(({ src, dest }) => {
    const babelBuild = () => babelify(src, dest);
    return babelBuild;
  });
  const run = gulp.parallel(...tasks);
  return run(() => {
    done();
  });
};

exports.watch = gulp.series(exports.clean, exports.build, (done) => {
  gulp.watch(flattenSrc(exports.paths), { delay: 1000 }, exports.build)
    .on('error', (err) => {
      console.log(err);
    });
  done();
});

function flattenSrc(paths) {
  return paths.reduce((arr, p) => arr.concat(p.src), []);
}

// build then live reload server
const dev = gulp.series(exports.build, liveServer('api', 'start:debug'));
exports.dev = dev;

exports.watch = gulp.series(exports.clean, dev, (done) => {
  gulp.watch([...exports.paths[0].src], { delay: 1000 }, dev)
    .on('error', (err) => {
      console.log(err);
    });
  done();
});


/**
 * Gulp 中使用，启动并重新启动 server。注意 export 的函数返回必须是一个 promise 或者支持一个回调函数（done）作为参数
 */
const childProcess = require('child_process');

// 由于在非 Windows 平台上启用了进程组，所以需要额外监听 ctrl+c，否则无法关闭子进程
process.on('SIGINT', () => {
  process.exit();
});

function liveServer(serverName, npmScript) {
  let child;
  process.on('exit', () => {
    if (child) kill(child);
  });

  return () => Promise.resolve().then(() => {
    if (child) {
      return kill(child);
    }
  }).then(() => {
    const isWin = process.platform === 'win32';
    child = childProcess.spawn(isWin ? 'npm.cmd' : 'npm', ['run', npmScript], {
      stdio: 'inherit',
      detached: !isWin, // 搞成一个进程组，方便后续对整个进程组进行 kill
    });
    child.on('close', (code) => {
      if (code === 8) {
        console.log('Error detected, waiting for changes...');
      } else {
        console.log(`server restarted for ${serverName}`);
      }
    });
    child.on('error', (err) => {
      console.log(err);
    });
  });
}

function kill(proc) {
  if (process.platform === 'win32') {
    return Promise.fromCallback(cb =>
      childProcess.exec(`taskkill /F /T /pid ${proc.pid}`, (err) => {
        if (err) console.log(err);
        cb();
      }));
  } else {
    // proc.kill();
    try {
      process.kill(-proc.pid); // 前面的负号表示这是一个进程组
    } catch (e) {
      // 如果错误是 kill ESRCH，则表示找不到进程组，说明前面可能没有启动成功，可以直接忽略。
      if (e.message.indexOf('kill ESRCH') < 0) {
        console.log(e);
      }
    }
  }
}

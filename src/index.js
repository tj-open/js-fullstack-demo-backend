
import express, { Router } from 'express';
import notFound from './middlewares/404';
import errorMiddleware from './middlewares/error';
import corsMiddleware from './middlewares/cors';
import bodyMiddleware from './middlewares/body';
import routers from './routers';
import config from './config';
import './lib/unhandled-error';

const app = express();

// Express security best practices: http://expressjs.com/en/advanced/best-practice-security.html
app.disable('x-powered-by');

// 禁止服务 /favicon.ico
app.all('/favicon.ico', (req, resp) => {
  resp.statusCode = 404;
  resp.end('Not Found');
});

// =============== BEGIN 拦截 API ==================
const apiRouter = Router();
apiRouter.use(corsMiddleware);
apiRouter.use(bodyMiddleware);
apiRouter.use(routers);
app.use(config.application.path, apiRouter); // 只对 config.server.path 下的 URL 进行拦截处理
// =============== END 拦截 API ==================

app.use(notFound);
app.use(errorMiddleware);

// 创建 HTTP Server
const server = require('http').Server(app);

const { ip, port } = config.application;
server.listen(port, ip, () => {
  console.log('started server, listening at %s:%s...', ip, port);
});

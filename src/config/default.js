const ms = require('ms');
const bytes = require('bytes');

module.exports = {
  application: { // 应用服务器配置
    ip: '127.0.0.1', // 监听的 IP 网卡地址
    port: 3000, // 监听的端口
    path: '/api', // 接口的路径前缀
    https: false, // 是否支持 HTTPS
    domain: '127.0.0.1', // 服务域名，影响到输出的 Cookie 的 domain
    middlewares: {
      compression: {
        enable: false, // 是否启用压缩
      },
      cors: {
        enable: true, // 是否启用 CORS 跨域访问
        origin: true,
        maxAge: ms('30d'), // 预检缓存最长时间（ms）
        methods: ['GET', 'POST'],
      },
      session: {
        enable: true, // 是否启用服务端会话支持
        name: 'jsfsid', // Cookie 名称，如 sid
        secret: 'helloworld', // 加密 Key
        ttl: ms('15min'), // 活跃会话存活时间
        store: {
          type: 'file', // 会话存储器类型，可以考虑用 redis 之类
          file: {
            dir: 'data/sessions', // 会话文件存储位置
          },
        },
      },
      body: {
        jsonBodyLimit: bytes('10kb'), // JSON 请求内容大小限制
      },
    },
  },
  database: {
    ip: '127.0.0.1', // 数据库 IP 地址
    port: 5432, // 数据库端口
    username: 'test', // 数据库用户名
    password: 'test', // 数据库密码
    poolMaxSize: 10, // 连接池最大容量
    poolMinSize: 1, // 连接池最小容量
    database: 'test', // 数据库名,
  },
};


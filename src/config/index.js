import _ from 'lodash';
import defaults from './default';
import local from './local';
import development from './development';
import test from './test';
import production from './production';

const config = defaults;
switch (process.env.DEPLOY_ENV) {
  case 'development': _.merge(config, development); break;
  case 'test': _.merge(config, test); break;
  case 'production': _.merge(config, production); break;
  default: _.merge(config, local);
}

module.exports = config;


import cors from 'cors';
import _ from 'lodash';
import ms from 'ms';
import config from '../config';

const defaultOptions = {
  origin: false, // cors origin, see https://github.com/expressjs/cors#configuration-options
  maxAge: ms('30d') / 1000, // 预检请求有效期（s)。默认 30天
  methods: ['POST'], // 允许的 HTTP 动词
  credentials: true, // 是否允许客户端保存和发送 Cookie。除了该选项要为 true，origin 还不能是 *，且 xhr 的 withCredentials = true
  // allowedHeaders: ['Date', ], // 客户端请求里可以额外带的 Header
  exposedHeaders: ['Date'], // 客户端可以访问响应里的哪些额外 Header
};

const options = _.merge(defaultOptions, config.application.middlewares.cors);
if (options.maxAge) options.maxAge /= 1000; // 传入的是毫秒，要转成秒

export default cors(options);


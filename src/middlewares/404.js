
const isLocalEnv = !process.env.DEPLOY_ENV || process.env.DEPLOY_ENV === 'local';

// 处理 404
export default (req, resp) => {
  if (isLocalEnv) {
    console.log('404', req.method, req.url); // eslint-disable-line no-console
  }

  const result = { errno: 404, errmsg: 'Not Found' };
  if (resp.headersSent) {
    resp.end(JSON.stringify(result));
  } else {
    resp.json(result);
  }
};

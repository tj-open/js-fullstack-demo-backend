
import session from 'express-session';
import initFileStore from 'session-file-store';
import config from '../config';

const isLocalEnv = !process.env.DEPLOY_ENV || process.env.DEPLOY_ENV === 'development';

const domain = config.application.domain;
const path = config.application.path;
const {
  name: cookieName, secret, ttl, store: storeOptions,
} = config.application.middlewares.session;

let store;
const { type, file: fileOptions } = storeOptions;
if (!type || type === 'file') {
  // Only for early development stage
  const FileStore = initFileStore(session);
  store = new FileStore({
    path: fileOptions && fileOptions.dir,
    ttl: ttl / 1000, // sec
    reapInterval: 5 * 60, // 清理线程间隔时间（sec）
    logFn: isLocalEnv ? console.log : () => null,
  });
} else {
  throw new Error(`unsupported session type: ${type}`);
}

export default session({
  store,
  secret,
  name: cookieName, // cookie name
  cookie: { // default httpOnly: true
    domain,
    path: path || '/',
    maxAge: ttl, // ms
  },
  resave: false, // 只有当设置了 maxAge 且 store 没有实现 touch 方法时才需要设置为 true
  rolling: true, // 重置 maxAge
  saveUninitialized: false, // 是否保存未初始化（新建但没有修改）的 session。一般就是说未登录前是否要保存 session（即创建 Cookie）
});


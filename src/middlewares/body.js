
import { Router } from 'express';
import bodyParser from 'body-parser';
import config from '../config';

const jsonBodyLimit = config.application.middlewares.body.jsonBodyLimit;

const router = Router();
export default router;

router.use(bodyParser.json({ limit: jsonBodyLimit }));
router.use(bodyParser.text({ type: '*/xml' }));
router.use(bodyParser.text({ type: 'text/*' }));



// 处理异常
export default (err, req, resp, next) => { // eslint-disable-line
  const url = req.originalUrl;

  console.log('unexpectly failed to request %s: %s', url, err.message, err);

  const result = { errno: 500, errmsg: '系统内部发生了点问题，请稍后尝试' };
  if (resp.headersSent) {
    resp.end(JSON.stringify(result));
  } else {
    resp.json(result);
  }
};

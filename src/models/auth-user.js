
import Sequelize from 'sequelize';
import db from '../lib/db';

const AuthUser = db.define('authUser', {
  id: {
    type: Sequelize.STRING(64),
    allowNull: false,
    primaryKey: true,
  },
  name: { // 姓名
    type: Sequelize.STRING(50),
    allowNull: false,
  },
  email: {
    type: Sequelize.STRING(75),
    allowNull: false,
    validate: {
      isEmail: {
        args: true,
        msg: '邮箱格式不对',
      },
    },
    unique: {
      msg: '员工邮箱已存在',
    },
  },
  password: {
    type: Sequelize.STRING(255),
    allowNull: true,
    validate: {
      notEmpty: {
        args: true,
        msg: '员工密码不能为空',
      },
    },
  },
  createdAt: {
    field: 'created_at',
    type: Sequelize.DATE,
    allowNull: false,
    defaultValue: db.literal('NOW()'),
  },
  updatedAt: {
    field: 'updated_at',
    type: Sequelize.DATE,
    allowNull: false,
    defaultValue: db.literal('NOW()'),
  },
  createdBy: {
    field: 'created_by',
    type: Sequelize.STRING(64),
    allowNull: false,
    defaultValue: 'system',
  },
  updatedBy: {
    field: 'updated_by',
    type: Sequelize.STRING(64),
    allowNull: false,
    defaultValue: 'system',
  },
}, {
  timestamps: true, // 自动填充 createdAt、updatedAt 的值
  underscoredAll: true, // 表名自动生成
  indexes: [
    { fields: ['email'] },
    { fields: ['created_at'] },
  ],
});

export default AuthUser;

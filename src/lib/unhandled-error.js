

process.on('uncaughtException', (err) => {
  const info = getErrorInfo(err);

  console.log('uncaughtException:', info);
  process.exit(1);
});


process.on('unhandledRejection', (err, p) => {
  const info = getErrorInfo(err);

  console.log('unhandledRejection:', info);
});

function getErrorInfo(err) {
  if (!err) return err;
  let info = '';
  if (err.stack) info = err.stack;
  else info = err.message || err.toString();

  if (err && err.name === 'ReplyError' && err.command) { // redis 错误，参考 lib/redis.js
    info = `Redis 操作（${err.command.name} ${JSON.stringify(err.command.args)}）失败：${info}`;
  }

  return info.replace(/\n/g, '\\n');
}


import Sequelize from 'sequelize';
import config from '../config';

const poolMaxSize = config.database.poolMaxSize;
const poolMinSize = config.database.poolMinSize;
const database = config.database.database;
const username = config.database.username;
const password = config.database.password;
const host = config.database.host;
const port = config.database.port;

const sequelize = new Sequelize(database, username, password, {
  dialect: 'postgres',
  host,
  port,

  pool: {
    max: poolMaxSize,
    min: poolMinSize,
    idle: 10000,
  },

  // The timezone used when converting a date from the database into a JavaScript date. The timezone is also used to SET TIMEZONE when connecting to the server, to ensure that the result of NOW, CURRENT_TIMESTAMP and other time related functions have in the right timezone.
  // 固定使用北京时区，因为后端（Java）的日期都是不带时区的北京时间。
  // 这个修改应该只影响所有需要用 now() 等函数的裸 SQL 执行。
  timezone: '+08:00',

  define: { // default options for all models
    // timestamps: true, // add createdAt and updatedAt attributes
    // paranoid: true, // Calling destroy will not delete the model, but instead set a  deletedAt timestamp if this is true. Needs timestamps=true to work
    // underscored: true, // use underscore style for automatically added attributes, like createdAt will be created_at(attribute and field both will be created_at). custom attribute should use field explicitly.
    // underscoredAll: true, // Converts camelCased model names to underscored table names if true. or you can use tableName in model definition options
    // freezeTableName: true, // disable automatically change table name using plural form

  },

  logging: false, // 禁止默认的 console.log，可对 sync() 等方法指定额外的 logging 来覆盖此选项
});

export default sequelize;


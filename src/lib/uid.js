
import { randomBytes } from 'crypto';

// use randomBytes, see https://github.com/kelektiv/node-uuid/blob/master/v4.js

// 只返回 32 位小写的 Id
export default function uid() {
  const buf = randomBytes(16);
  return buf.toString('hex');
}


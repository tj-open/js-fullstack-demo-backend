/**
 * 支持 async/await route，如果 route 抛出异常，会调用 next(err)。其他跟原生的 router 没区别。
 *
 * 使用方法：
 * router.use('/xxx', async(req, resp, next)=>{
 *   const data = await doSomethingWithPromise();
 *   resp.send(data)
 * })
 */
import { Router } from 'express';
import http from 'http';

const methods = http.METHODS.map(m => m.toLowerCase()).concat('use', 'all');

export default function ExtendRouter(...routerArgs) {
  const router = Router(...routerArgs);
  methods.forEach((method) => {
    if (!router[method]) return;
    const orgRouteMethod = router[method];
    if (typeof orgRouteMethod !== 'function') return;

    router[method] = (...args) => {
      args = args.map((arg) => {
        if (typeof arg !== 'function') return arg;
        return (req, resp, next) => {
          try {
            Promise.resolve(arg(req, resp, next)).catch(next);
          } catch (e) {
            next(e);
          }
        };
      });
      return orgRouteMethod.call(router, ...args);
    };
  });

  return router;
}

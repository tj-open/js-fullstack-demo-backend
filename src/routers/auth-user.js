import Router from '../lib/router';
import AuthUser from '../models/auth-user';
import uid from '../lib/uid';

const router = new Router();
export default router;

// 登录
router.all('/v1/login', async (req, resp) => {
  const { email, password } = req.body;

  const user = await AuthUser.findOne({
    attributes: ['id', 'name'],
    where: { email, password },
  });
  if (!user) {
    resp.send({ errno: 400, errmsg: '用户名或密码错误' });
    return;
  }

  // 重新生成 Session 会话
  await Promise.fromCallback(cb => req.session.regenerate(cb));

  resp.send({ errno: 0, errmsg: null, data: user.toJSON() });
});


// 查询用户列表
router.all('/v1/user/query', async (req, resp) => {
  const { term, offset, limit } = req.body;

  const where = {};
  if (term) where.name = { $ilike: `%${term}%` };

  const users = await AuthUser.findAll({
    attributes: ['id', 'name', 'email'],
    where,
    offset,
    limit,
  });

  resp.send({ errno: 0, errmsg: null, data: users.map(user => user.toJSON()) });
});

// 新增用户
router.all('/v1/user/create', async (req, resp) => {
  const { name, email, password } = req.body;

  const user = await AuthUser.create({
    id: uid(), name, email, password,
  });

  resp.send({ errno: 0, errmsg: null, data: user.toJSON() });
});

// 更新用户
router.all('/v1/user/update', async (req, resp) => {
  const {
    id, name, email, password,
  } = req.body;

  const user = await AuthUser.findOne({
    attributes: ['id', 'name'],
    where: { id },
  });
  if (!user) {
    resp.send({ errno: 400, errmsg: '用户不存在' });
    return;
  }

  user.name = name;
  user.email = email;
  user.password = password;
  await user.save();

  resp.send({ errno: 0, errmsg: null, data: user.toJSON() });
});


// 删除用户
router.all('/v1/user/remove', async (req, resp) => {
  const {
    id,
  } = req.body;

  const user = await AuthUser.findOne({
    attributes: ['id', 'name'],
    where: { id },
  });
  if (!user) {
    resp.send({ errno: 400, errmsg: '用户不存在' });
    return;
  }

  await user.destroy();

  resp.send({ errno: 0, errmsg: null });
});

module.exports = {
  "env": {
    "mocha": true
  },
  "globals": {
    "times": true,
    "mock": true,
    "nock": true
  },
  "settings": {
    "import/resolver": {
      "node": {
        "moduleDirectory": ["node_modules", "src"]
      }
    }
  },
  "rules": {
    "prefer-arrow-callback": "off",
    "func-names": "off",
    "import/no-unresolved": "off",
    "import/no-extraneous-dependencies": "off"
  }
}